<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CarrosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $carros = [
            0=>[
                'nome'=>'Gol',
                'placa'=>'hue-0670'
            ],
            1=>[
                'nome'=>'Celta',
                'placa'=>'xvi-3459'
            ]
        ];
        DB::table('carros')->insert($carros);
    }
}
