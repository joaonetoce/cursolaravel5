<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\Painel\Carro;

class CarrosController extends Controller
{
    public function getIndex()
    {
        $carros = Carro::get(); //or all()
        //dd($carros);
        return View('painel.carros.index', compact('carros'));
    }

    public function getAdicionar()
    {
        return View('painel.carros.create-edit');
    }

    public function getEditar($idCarro)
    {
        return View('painel.carros.create-edit', ['idCarro'=>$idCarro]);
    }

    public function postEditar($array = array())
    {
        return "Editando o carro...";
    }

    public function getDeletar($idCarro)
    {
        return "Deletando o carro";
    }

    public function getListaCarrosLuxo()
    {
        return "Listando os carros de luxo";
    }

    public function missingMethod($parameters = array())
    {
        return 'ERRO 404, Página não encontrada';
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
