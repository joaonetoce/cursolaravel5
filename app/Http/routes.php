<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('produtos', 'ProdutoController@index');
Route::get('produto/create', 'ProdutoController@create');
Route::post('produto/create', 'ProdutoController@store');
Route::get('produto/{idProd}', 'ProdutoController@show');
//Route::get('produto/{idProd}/{idProd2}', 'ProdutoController@showTwo');
Route::get('produto/edit/{idProd}', 'ProdutoController@edit');

Route::controller('carros', 'CarrosController');
Route::controller('carros/adicionar', 'CarrosController');
Route::controller('carros/editar/{idCar}', 'CarrosController');
Route::controller('carros/lista-carros-luxo', 'CarrosController');

/*
Route::get('/contato', function (){
    return 'Página de Contato';
});

Route::post('cadastrar/user', function (){
    return 'Cadastrando usuário...';
});

Route::match(['post', 'get'], '/match', function (){
    return 'Minha rota Match';
});

Route::Any('any', function(){
    return 'Rota do tipo Any';
});

Route::get('produto/editar/{idProduto}', function($idProduto){
    return "Editar o produto => {$idProduto}";
})->where('idProduto', '[0-9]+');

Route::get('produto/delete/{idProduto?}', function($idProduto = ''){
    return "Deletar o produto => {$idProduto}";
});

Route::get('produto/{idProduto}/imagem/{idImagem}', function ($idProduto, $idImagem){
    return "Produto => {$idProduto}, e Imagem => {$idImagem}";
});

Route::group(['prefix'=>'painel'], function (){
    Route::get('financeiro', function(){
        return 'financeiro do painel';
    });

    Route::get('usuarios', function(){
        return 'Usuário';
    });
});

Route::group(['prefix'=>'painel', 'middleware'=>'my-middleware'], function (){
    //Route::get('/', function(){
    //    return 'Dashboard do painel';
    //});

    Route::get('/', function(){
        return View('painel.home.index');
    });

    Route::get('financeiro', function(){
        return 'financeiro do painel';
    });

    Route::get('usuarios', function(){
        return 'Usuário';
    });
});

Route::get('/login', function(){
    return 'Form de Login';
});
 * */

